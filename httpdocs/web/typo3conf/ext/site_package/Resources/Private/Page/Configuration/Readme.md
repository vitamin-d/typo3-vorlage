Icons for the individual backend layouts need to be placed in Resources/Public,
otherwise webserver configuration might prevent to use them via HTTP.
