<?php
namespace VITD\SitePackage\View;

use TYPO3\CMS\Backend\View\BackendLayout\BackendLayout as CoreBackendLayout;
use TYPO3\CMS\Core\TypoScript\Parser\TypoScriptParser;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;


/**
 * BackendLayout with support for page tree depth based restrictions
 *
 * The restrictions are defined inside the BackendLayout configuration:
 *
 * <code>
 * backend_layout {
 *     minLevel = 2
 *     maxLevel = 4
 *     …
 * }
 * </code>
 *
 * where minLevel is the minimum page depth (counting starts with zero at site-root pages) for this BackendLayout to
 * be allowed, maxLevel the respective maximum. Both values are optional.
 *
 * Defaults:
 * <code>
 * minLevel = 0
 * # maxLevel not set.
 * </code>
 *
 * @author Ludwig Rafelsberger <ludwig.rafelsberger@vitd.at>
 */
class BackendLayout extends CoreBackendLayout
{

    /**
     * Minimum page nesting level for this BackendLayout to be available
     *
     * @var int
     */
    protected $minLevel = 0;

    /**
     * Maximum page nesting level for this BackendLayout to be available
     *
     * @var null|int
     */
    protected $maxLevel;




    /**
     * Create a new BackendLayout
     *
     * @param string $identifier Identifier
     * @param string $title Title
     * @param string $configuration Actual BackendLayout configuration
     *
     * @return BackendLayout The newly created BackendLayout
     */
    public static function create($identifier, $title, $configuration): BackendLayout
    {
        return GeneralUtility::makeInstance(
            self::class,
            $identifier,
            $title,
            $configuration
        );
    }


    /**
     * Create a new Backendlayout
     *
     * @param string $identifier Identifier
     * @param string $title Title
     * @param string $configuration Actual BackendLayout configuration
     */
    public function __construct($identifier, $title, $configuration)
    {
        parent::__construct($identifier, $title, $configuration);

        $parser = GeneralUtility::makeInstance(TypoScriptParser::class);
        $parser->parse($configuration);
        if (!empty($parser->setup['backend_layout.']['minLevel'])) {
            $this->minLevel = (int)$parser->setup['backend_layout.']['minLevel'];
        }
        if (!empty($parser->setup['backend_layout.']['maxLevel'])) {
            $this->maxLevel = (int)$parser->setup['backend_layout.']['maxLevel'];
        }
    }


    /**
     * Test whether this BackendLayout is allowed on the given page
     *
     * @param int $pageUid Uid of the page to test for allowance
     *
     * @return bool true if this BackendLayout is allowed on the given page, false otherwise
     */
    public function isAllowed($pageUid): bool
    {
        $depth = \count(GeneralUtility::makeInstance(RootlineUtility::class, $pageUid)->get()) - 1;
        return $this->minLevel <= $depth && (null === $this->maxLevel || $depth <= $this->maxLevel);
    }
}
