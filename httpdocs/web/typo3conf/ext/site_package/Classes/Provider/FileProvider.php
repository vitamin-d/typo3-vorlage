<?php
namespace VITD\SitePackage\Provider;

use VITD\SitePackage\View\BackendLayout;
use TYPO3\CMS\Backend\View\BackendLayout\BackendLayoutCollection;
use TYPO3\CMS\Backend\View\BackendLayout\DataProviderContext;
use TYPO3\CMS\Backend\View\BackendLayout\DataProviderInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * File-based BackendLayout provider
 *
 * @author Ludwig Rafelsberger <ludwig.rafelsberger@vitd.at>
 */
class FileProvider implements DataProviderInterface
{

    /**
     * Add backend layouts to the given backend layout collection.
     *
     * @param DataProviderContext $dataProviderContext
     * @param BackendLayoutCollection $backendLayoutCollection
     *
     * @return void
     */
    public function addBackendLayouts(
        DataProviderContext $dataProviderContext,
        BackendLayoutCollection $backendLayoutCollection
    )
    {
        $files = $this->getFiles();
        foreach ($files as $file) {
            $backendLayout = $this->createBackendLayout($file);
            if ($backendLayout->isAllowed($dataProviderContext->getPageId())) {
                $backendLayoutCollection->add($backendLayout);
            }
        }
    }


    /**
     * Get a backend layout by (regular) identifier.
     *
     * @param string $identifier
     * @param integer $pageId
     *
     * @return null|BackendLayout
     */
    public function getBackendLayout($identifier, $pageId)
    {
        $file = GeneralUtility::getFileAbsFileName(
            'EXT:site_package/Resources/Private/Page/Configuration/' . $identifier . '.ts'
        );
        if (!is_readable($file)) {
            return null;
        }
        $backendLayout = $this->createBackendLayout($file);
        return $backendLayout->isAllowed($pageId)
            ? $backendLayout
            : null;
    }





    // ---------------------- internal helper methods -----------------------
    /**
     * Get BackendLayout definition files
     *
     * @return array List of (string) full path filenames of BackendLayout definition files
     */
    protected function getFiles(): array
    {
        $collection = [];

        $path = GeneralUtility::getFileAbsFileName('EXT:site_package/Resources/Private/Page/Configuration/');

        $files = GeneralUtility::getFilesInDir($path, 'ts', true, 1);
        foreach ($files as $file) {
            $collection[sha1($file)] = $file;
        }

        return $collection;
    }


    /**
     * Create a new backend layout from a BackendLayout definition file
     *
     * @param string $file Full path to BackendLayout definition file
     *
     * @return BackendLayout The BackendLayout as defined by $file and additional files named by convention
     */
    protected function createBackendLayout($file): BackendLayout
    {
        $fileInformation = pathinfo($file);
        $content = GeneralUtility::getUrl($file);

        $backendLayout = BackendLayout::create(
            $fileInformation['filename'],
            $fileInformation['filename'],
            $content
        );

        $iconfile = GeneralUtility::getFileAbsFileName(
            'EXT:site_package/Resources/Public/BackendLayouts/' . $fileInformation['filename'] . '.png'
        );
        if (is_readable($iconfile)) {
            $iconfile = '../' . str_replace(PATH_site, '', $iconfile);
            $backendLayout->setIconPath($iconfile);
        }

        return $backendLayout;
    }
}
