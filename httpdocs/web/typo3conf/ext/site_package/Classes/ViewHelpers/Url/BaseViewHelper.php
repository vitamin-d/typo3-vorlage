<?php
namespace VITD\SitePackage\ViewHelpers\Url;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;


/**
 * View helper to determine the current base url.
 *
 * <code title="Example">
 * <m:url.base />
 * </code>
 * <output>
 * http://yourdomain.tld/
 * (depending on your domain)
 * </output>
 */
class BaseViewHelper extends AbstractViewHelper
{

    use CompileWithRenderStatic;

    /**
     * Output the base url
     *
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     *
     * @return string base url
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        return htmlspecialchars($renderingContext->getControllerContext()->getRequest()->getBaseUri());
    }
}
