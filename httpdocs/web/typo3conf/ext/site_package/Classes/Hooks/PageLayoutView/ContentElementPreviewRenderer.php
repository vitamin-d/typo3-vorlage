<?php
namespace VITD\SitePackage\Hooks\PageLayoutView;

use \TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use \TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Backend\Utility\BackendUtility;


/**
 * Preview rendering for the page module of our own content elements
 */
class ContentElementPreviewRenderer implements PageLayoutViewDrawItemHookInterface
{

    /**
     * Preprocess the preview rendering of our own content elements
     *
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject Calling parent object
     * @param bool $drawItem Whether to draw the item using the default functionality
     * @param string $headerContent Header content
     * @param string $itemContent Item content
     * @param array $row Record row of tt_content
     *
     * @return void
     */
    public function preProcess(
        PageLayoutView &$parentObject,
        &$drawItem,
        &$headerContent,
        &$itemContent,
        array &$row
    ) {
        $config = BackendUtility::getPagesTSconfig(0);
        $wizardArray = $config['mod.']['wizards.']['newContentElement.']['wizardItems.']['common.']['elements.'];

        $contentType = $wizardArray[str_replace('sitepackage_', '', $row['CType']) . '.']['title'];
        $titleSet = false;
        if (\strlen(trim($contentType)) > 0) {
            $headerContent = $contentType . '<br><b>' . $row['header'] . '</b></br>';
            $titleSet = true;
        } else {

            $wizardArray = $config['mod.']['wizards.']['newContentElement.']['wizardItems.']['plugins.']['elements.'];
            foreach ($wizardArray as $element) {
                if ($element['tt_content_defValues.']['CType'] === $row['CType']) {
                    $headerContent = 'Plugin ' . $contentType . '<br><b>' . $element['title'] . ': ' . $element['description'] . '</b>';
                    $titleSet = true;
                }
            }
        }

        $drawItem = !$titleSet;
    }
}
