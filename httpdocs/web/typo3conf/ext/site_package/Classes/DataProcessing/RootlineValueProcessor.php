<?php
namespace VITD\SitePackage\DataProcessing;

use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * Obtain page attribute(s) from the nearest matching page
 *
 * DataProcessor to obtain a page attribute from the nearest page up that has this attribute set (non-empty/php trueish).
 *
 * Example TypoScript configuration
 *
 * 10 = VITD\SitePackage\DataProcessing\RootlineValueProcessor
 * 10 {
 *     # this is the one page attribute that is returned
 *     field = nav_title
 *
 *     # Variable that is made available (inserted/overwritten) to the view
 *     as = nav_title
 * }
 */
class RootlineValueProcessor implements DataProcessorInterface
{

    /**
     * Obtain page attribute(s) from the nearest matching page
     *
     * @param ContentObjectRenderer $cObj The data of the content element or page
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     *
     * @return array the processed data as key/value store
     */
    public function process(ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration, array $processedData): array
    {
        $field = $cObj->stdWrapValue('field', $processorConfiguration, null);
        $result = $cObj->rootLineValue(
            \call_user_func(function() {
                $t = $this->getFrontendController()->tmpl->rootLine;
                end($t);
                return key($t);
            }),
            $field,
            true
        );

        $targetVariableName = $cObj->stdWrapValue('as', $processorConfiguration);
        $processedData[$targetVariableName] = $result;

        return $processedData;
    }






    // ------------------------ global object access ------------------------
    /**
     * Get the TypoScript Frontend rendering engine
     *
     * @return \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController The frontend engine as stored in
     *     $GLOBALS['TSFE']
     */
    protected function getFrontendController(): TypoScriptFrontendController
    {
        return $GLOBALS['TSFE'];
    }
}
