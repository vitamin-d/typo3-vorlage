<?php

namespace VITD\SitePackage\DataProcessing;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Frontend\Resource\FileCollector;

/**
 * DataProcessor to obtain the first file from a relation to sys_file records (e.g. sys_file_reference records).
 *
 * Example TypoScript configuration (with defaults as values)
 *
 * 10 = VITD\SitePackage\DataProcessing\FileProcessor
 * 10 {
 *     fieldName = assets
 *     table = tt_content
 *     as = file
 * }
 * @author Ludwig Rafelsberger <ludwig.rafelsberger@vitd.at>
 */
class FileProcessor implements DataProcessorInterface
{
    /**
     * Process data of a record to resolve File objects to the view
     *
     * @param ContentObjectRenderer $cObj The data of the content element or page
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     *
     * @return array the processed data as key/value store
     */
    public function process(ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration, array $processedData): array
    {

        // gather data
        $fileCollector = GeneralUtility::makeInstance(FileCollector::class);
        $relationField = $cObj->stdWrapValue('fieldName', $processorConfiguration, 'assets');
        $relationTable = $cObj->stdWrapValue('table', $processorConfiguration, $cObj->getCurrentTable());

        if ($relationTable === 'pages' && $this->getFrontendController()->sys_language_uid > 0) {
            $fileCollector->addFilesFromRelation('pages_language_overlay', $relationField, $cObj->data);
        }

        $fileCollector->addFilesFromRelation($relationTable, $relationField, $cObj->data);

        // set the first file into a variable, default "file"
        $targetVariableName = $cObj->stdWrapValue('as', $processorConfiguration, 'file');
        $processedData[$targetVariableName] = current($fileCollector->getFiles());

        return $processedData;
    }





    // ------------------------ global object access ------------------------
    /**
     * Get the TypoScript Frontend rendering engine
     *
     * @return \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController The frontend engine as stored in
     *     $GLOBALS['TSFE']
     */
    protected function getFrontendController()
    {
        return $GLOBALS['TSFE'];
    }
}
