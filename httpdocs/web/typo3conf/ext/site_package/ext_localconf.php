<?php
defined('TYPO3_MODE') or die();

(function ($packageKey) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $packageKey . '/Configuration/PageTS/TCEFORM.txt">'
    );


    if (TYPO3_MODE === 'BE') {
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['BackendLayoutDataProvider']['vitd'] =
            VITD\SitePackage\Provider\FileProvider::class;


        // register content element typeicons
        foreach (
            new \DirectoryIterator(
                \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('EXT:site_package/Resources/Public/Icons/ContentElements/')
            )
            as $fileinfo
        ) {
            if (!in_array($fileinfo->getType(), ['file', 'link']) || !in_array($fileinfo->getExtension(), ['svg', 'png', 'jpg', 'gif'])) {
                continue;
            }

            \TYPO3\CMS\Core\Utility\GeneralUtility
                ::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class)
                ->registerIcon(
                    'sitepackage_' . mb_strtolower($fileinfo->getBasename('.' . $fileinfo->getExtension()), 'utf8'),
                    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                    ['source' => $fileinfo->getRealPath()]
                );

            $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']
            ['sitepackage_' . mb_strtolower($fileinfo->getBasename('.' . $fileinfo->getExtension()), 'utf8')] =
                VITD\SitePackage\Hooks\PageLayoutView\ContentElementPreviewRenderer::class;
        }
    }

    $GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['SitePackage'] = 'EXT:' . $packageKey . '/Configuration/RTE/SitePackage.yaml';
})('site_package');
