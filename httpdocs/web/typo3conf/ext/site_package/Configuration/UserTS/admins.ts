<INCLUDE_TYPOSCRIPT: source="FILE: EXT:site_package/Configuration/UserTS/_all.ts">

options {

	# Allow System Cache access
	clearCache.system = 1
}

TCAdefaults.be_users.workspace_perms = 0
