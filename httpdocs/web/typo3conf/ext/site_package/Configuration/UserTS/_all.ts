#
# Base configuration for all groups
#

#
# TCA defaults
#
options {
	clearCache {
		all = 1
		pages = 1
	}
	createFoldersInEB = 1
}
