<?php
defined('TYPO3_MODE') or die();

(function (string $packageKey, string $table) {
    $extRelPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($packageKey);

    // avoid "[Translate to english]: " prefixes
    $GLOBALS['TCA'][$table]['columns']['header']['l10n_mode'] = '';
    $GLOBALS['TCA'][$table]['columns']['bodytext']['l10n_mode'] = '';


    // ------------------------- additional columns -------------------------



    // ------------------ preparation for content elements ------------------
    /**
     * Add parts of a new content element type definition
     *
     * It is expected that a SVG icon with the same name as the page type name is placed under
     * Resources/Public/Icons/ContentElements/ (First character uppercased).
     *
     * @param string $name Name of the content element type
     *      ([A-Z][a-z]+ - first character uppercase, all other lowercase)
     *
     * @return void
     */
    $addContentElementType = function (string $name) use ($packageKey, $table, $extRelPath) {
        $key = mb_strtolower(str_replace('_', '', $packageKey) . '_' . $name);

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
            [
                ucfirst($name),
                $key,
                $extRelPath . 'Resources/Public/Icons/ContentElements/' . ucfirst($name) . '.svg',
            ],
            'CType',
            $packageKey
        );
        $GLOBALS['TCA'][$table]['ctrl']['typeicon_classes'][$key] = $key;
    };


    // -------------------------- content elements --------------------------

})('site_package', 'tt_content');
