<?php
$EM_CONF['site_package'] = [
    'title' => 'Website',
    'description' => 'Site package for this website',
    'category' => 'misc',
    'author' => 'Ludwig Rafelsberger',
    'author_email' => 'ludwig.rafelsberger@vitd.at',
    'author_company' => 'VITAMIN D GmbH',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'assets' => '1.2.0-1.99.99',
            'columns' => '87.1.0-87.99.99',
            'fix_imageviewhelper' => '8.7.0-8.7.99',
            'fluid_styled_content' => '8.7.0-8.7.99',
            'l10n' => '1.0.0-1.0.99',
            'typo3' => '8.7.0-8.7.99',
            'realurl' => '2.3.0-2.99.99',
            'unroll' => '2.0.0-2.99.99',
            'vhs' => '5.0.0-5.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
