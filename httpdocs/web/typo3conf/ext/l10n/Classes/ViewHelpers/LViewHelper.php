<?php
namespace VITD\L10n\ViewHelpers;

use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Exception as ViewHelperException;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * L (Localisation) ViewHelper
 *
 * An even more extremely shortened alternative to f:translate.
 * <ul>
 *   <li>automatically outputs the name of the LLL reference if it is not found</li>
 *   <li>defaults to EXT:l10n, thereby collecting all LLL references in one single ext</li>
 * </ul>
 *
 * ### Examples
 *     <l:l>some.label</v:l>
 *     <l:l key="some.label" />
 *     <l:l arguments="{0: 'foo', 1: 'bar'}">some.label</l:l>
 *
 * @author Ludwig Rafelsberger <ludwig.rafelsberger@vitd.at>, VITAMIN D GmbH
 */
class LViewHelper extends AbstractViewHelper
{

    use CompileWithRenderStatic;

    /**
     * Initialize Arguments
     *
     * @throws \TYPO3Fluid\Fluid\Core\ViewHelper\Exception
     */
    public function initializeArguments()
    {
        $this
            ->registerArgument('key', 'string', 'Translation Key')
            ->registerArgument('id', 'string', 'Translation Key compatible to TYPO3 Flow')
            ->registerArgument(
                'default',
                'string',
                'If the given locallang key could not be found, this value is used. If this argument is not set, ' .
                'child nodes will be used to render the default'
            )
            ->registerArgument(
                'htmlEscape',
                'bool',
                'TRUE if the result should be htmlescaped. This won\'t have an effect for the default value'
            )
            ->registerArgument('arguments', 'array', 'Arguments to be replaced in the resulting string')
            ->registerArgument(
                'extensionName',
                'string',
                'UpperCamelCased extension key (for example BlogExample)',
                false,
                'L10n'
            );
    }


    /**
     * Render translation
     *
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     *
     * @throws \TYPO3Fluid\Fluid\Core\ViewHelper\Exception 1351584844 If neither "key" nor "id" argument is provided
     * @return string
     */
    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $key = $arguments['key'];
        $id = $arguments['id'];
        $default = $arguments['default'];
        $htmlEscape = $arguments['htmlEscape'];
        $extensionName = $arguments['extensionName'];
        $arguments = $arguments['arguments'];

        // Wrapper including a compatibility layer for TYPO3 Flow Translation
        if ($id === null) {
            $id = $key;
        }

        if ((string)$id === '') {
            throw new ViewHelperException('An argument "key" or "id" has to be provided', 1351584844);
        }

        $request = $renderingContext->getControllerContext()->getRequest();
        $extensionName = $extensionName === null ? $request->getControllerExtensionName() : $extensionName;
        $value = static::translate($id, $extensionName, $arguments);
        if ($value === null) {
            $value = $default !== null ? $default : $renderChildrenClosure();
            if (!empty($arguments)) {
                $value = vsprintf($value, $arguments);
            }
        } elseif ($htmlEscape) {
            $value = htmlspecialchars($value);
        }

        return $value;
    }

    /**
     * Wrapper call to static LocalizationUtility
     *
     * @param string $id Translation Key compatible to TYPO3 Flow
     * @param string $extensionName UpperCamelCased extension key (for example BlogExample)
     * @param array $arguments Arguments to be replaced in the resulting string
     *
     * @return NULL|string
     */
    protected static function translate($id, $extensionName, $arguments)
    {
        return LocalizationUtility::translate($id, $extensionName, $arguments);
    }
}
