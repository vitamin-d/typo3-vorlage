<?php
$EM_CONF['l10n'] = [
    'title' => 'Language Labels',
    'description' => 'Static texts, their translations and a ViewHelper to shorten fluid templates',
    'category' => 'misc',
    'author' => 'Ludwig Rafelsberger',
    'author_email' => 'ludwig.rafelsberger@vitd.at',
    'author_company' => 'VITAMIN D GmbH',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.7.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
