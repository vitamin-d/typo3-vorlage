<?php
# PackageStates.php

# This file is maintained by TYPO3's package management. Although you can edit it
# manually, you should rather use the extension manager for maintaining packages.
# This file will be regenerated automatically if it doesn't exist. Deleting this file
# should, however, never become necessary if you use the package commands.

return [
    'packages' => [
        'core' => [
            'packagePath' => 'typo3/sysext/core/',
        ],
        'extbase' => [
            'packagePath' => 'typo3/sysext/extbase/',
        ],
        'fluid' => [
            'packagePath' => 'typo3/sysext/fluid/',
        ],
        'install' => [
            'packagePath' => 'typo3/sysext/install/',
        ],
        'frontend' => [
            'packagePath' => 'typo3/sysext/frontend/',
        ],
        'fluid_styled_content' => [
            'packagePath' => 'typo3/sysext/fluid_styled_content/',
        ],
        'info' => [
            'packagePath' => 'typo3/sysext/info/',
        ],
        'info_pagetsconfig' => [
            'packagePath' => 'typo3/sysext/info_pagetsconfig/',
        ],
        'extensionmanager' => [
            'packagePath' => 'typo3/sysext/extensionmanager/',
        ],
        'lang' => [
            'packagePath' => 'typo3/sysext/lang/',
        ],
        'linkvalidator' => [
            'packagePath' => 'typo3/sysext/linkvalidator/',
        ],
        'setup' => [
            'packagePath' => 'typo3/sysext/setup/',
        ],
        'rte_ckeditor' => [
            'packagePath' => 'typo3/sysext/rte_ckeditor/',
        ],
        'taskcenter' => [
            'packagePath' => 'typo3/sysext/taskcenter/',
        ],
        'sys_action' => [
            'packagePath' => 'typo3/sysext/sys_action/',
        ],
        'func' => [
            'packagePath' => 'typo3/sysext/func/',
        ],
        'wizard_crpages' => [
            'packagePath' => 'typo3/sysext/wizard_crpages/',
        ],
        'backend' => [
            'packagePath' => 'typo3/sysext/backend/',
        ],
        'beuser' => [
            'packagePath' => 'typo3/sysext/beuser/',
        ],
        'cshmanual' => [
            'packagePath' => 'typo3/sysext/cshmanual/',
        ],
        'filelist' => [
            'packagePath' => 'typo3/sysext/filelist/',
        ],
        'filemetadata' => [
            'packagePath' => 'typo3/sysext/filemetadata/',
        ],
        'lowlevel' => [
            'packagePath' => 'typo3/sysext/lowlevel/',
        ],
        'opendocs' => [
            'packagePath' => 'typo3/sysext/opendocs/',
        ],
        'recordlist' => [
            'packagePath' => 'typo3/sysext/recordlist/',
        ],
        'recycler' => [
            'packagePath' => 'typo3/sysext/recycler/',
        ],
        'reports' => [
            'packagePath' => 'typo3/sysext/reports/',
        ],
        'saltedpasswords' => [
            'packagePath' => 'typo3/sysext/saltedpasswords/',
        ],
        'scheduler' => [
            'packagePath' => 'typo3/sysext/scheduler/',
        ],
        'sv' => [
            'packagePath' => 'typo3/sysext/sv/',
        ],
        'sys_note' => [
            'packagePath' => 'typo3/sysext/sys_note/',
        ],
        'tstemplate' => [
            'packagePath' => 'typo3/sysext/tstemplate/',
        ],
        'viewpage' => [
            'packagePath' => 'typo3/sysext/viewpage/',
        ],
        'assets' => [
            'packagePath' => 'typo3conf/ext/assets/',
        ],
        'columns' => [
            'packagePath' => 'typo3conf/ext/columns/',
        ],
        'fix_imageviewhelper' => [
            'packagePath' => 'typo3conf/ext/fix_imageviewhelper/',
        ],
        'l10n' => [
            'packagePath' => 'typo3conf/ext/l10n/',
        ],
        'realurl' => [
            'packagePath' => 'typo3conf/ext/realurl/',
        ],
        'unroll' => [
            'packagePath' => 'typo3conf/ext/unroll/',
        ],
        'vhs' => [
            'packagePath' => 'typo3conf/ext/vhs/',
        ],
        'site_package' => [
            'packagePath' => 'typo3conf/ext/site_package/',
        ],
    ],
    'version' => 5,
];
