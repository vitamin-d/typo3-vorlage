<?php
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = [
    'www.typo3-vorlage.at' => [
        'init' => [
            'appendMissingSlash' => 'ifNotFile,redirect',
            'emptyUrlReturnValue' => '/',
        ],
        'pagePath' => [
            'rootpage_id' => 1,
        ],
        'fileName' => [
            'defaultToHTMLsuffixOnPrev' => false,
            'acceptHTMLsuffix' => true,
        ],
    ],
    'dev.www.typo3-vorlage.at' => 'www.typo3-vorlage.at',
    'stage.www.typo3-vorlage.at' => 'www.typo3-vorlage.at',
];
