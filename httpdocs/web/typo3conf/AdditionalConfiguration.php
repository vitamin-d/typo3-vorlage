<?php
defined('TYPO3_MODE') or die();

/*
      dP                                  dP            dP                   dP                    dP
      88                                  88            88                   88                    88
.d888b88 .d8888b.     88d888b. .d8888b. d8888P    .d888b88 .d8888b. 88d888b. 88 .d8888b. dP    dP  88
88'  `88 88'  `88     88'  `88 88'  `88   88      88'  `88 88ooood8 88'  `88 88 88'  `88 88    88  dP
88.  .88 88.  .88     88    88 88.  .88   88      88.  .88 88.  ... 88.  .88 88 88.  .88 88.  .88
`88888P8 `88888P'     dP    dP `88888P'   dP      `88888P8 `88888P' 88Y888P' dP `88888P' `8888P88  oo
                                                                    88                        .88
                                                                    dP                    d8888P

 Notes:
    - This file is only for local development environments. This file resides already on all affected
      servers (live, stage, …) with the respective data.
    - If you have changes that are relevant for all developers, put it in here
    - If you need temporary changes, put them in here BUT DO NOT COMMIT THEM!!
    - The first developer who needs permanent changes for themselves only:
         - create a "if-another-extra-file-exists-then-include/require-it" php construct here at the end (allows overriding)
         - exclude that extra file from git (commit the respective .gitignore, so that it's effective for everyone)
         - rewrite this comment or remove it altogether
*/

(function() {

    $GLOBALS['TYPO3_CONF_VARS']['BE']['compressionLevel'] = 9;
    $GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = true;
    $GLOBALS['TYPO3_CONF_VARS']['BE']['installToolPassword'] = '$P$Cl55ig00OVF0AFRQOBgbgz0WVgjK4p/'; // dev
    $GLOBALS['TYPO3_CONF_VARS']['BE']['versionNumberInFilename'] = true;

    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['charset'] = 'utf8';
    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['dbname'] = 'dev';
    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['driver'] = 'mysqli';
    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['host'] = 'localhost';
    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['password'] = 'dev';
    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['port'] = 3306;
    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['unix_socket'] = '';
    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['user'] = 'dev';

    $GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] = true;
    $GLOBALS['TYPO3_CONF_VARS']['FE']['compressionLevel'] = 9;
    $GLOBALS['TYPO3_CONF_VARS']['FE']['versionNumberInFilename'] = 'embed';

    $GLOBALS['TYPO3_CONF_VARS']['GFX']['gdlib_png'] = false;
    $GLOBALS['TYPO3_CONF_VARS']['GFX']['jpg_quality'] = 90;
    $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor'] = 'GraphicsMagick';
    $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_allowTemporaryMasksAsPng'] = false;
    $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_colorspace'] = 'RGB';
    $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_effects'] = -1;
    $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_enabled'] = true;
    $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_path'] = '/usr/bin/';
    $GLOBALS['TYPO3_CONF_VARS']['GFX']['processor_path_lzw'] = '/usr/bin/';

    $GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport'] = 'mbox';
    $GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_mbox_file'] = '/vagrant/var/mail/mbox';
    $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'] = 'vagrant@dev.www.typo3-vorlage.at';
    $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromName'] = 'TYPO3 Vorlage Development Environment';

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['UTF8filesystem'] = true;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['extbase_object'] = [
        'backend' => TYPO3\CMS\Core\Cache\Backend\Typo3DatabaseBackend::class,
        'frontend' => TYPO3\CMS\Core\Cache\Frontend\VariableFrontend::class,
        'groups' => ['system'],
        'options' => ['defaultLifetime' => 0],
    ];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['createGroup'] = 'http';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = 1;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = 'file';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey'] = 'dev';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['fileCreateMask'] = '0664';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['folderCreateMask'] = '2775';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] = '[DEV] ' . $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLocale'] = 'de_DE';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLog'] = 'file,/vagrant/var/log/typo3.log';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLogLevel'] = 0;
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['trustedHostsPattern'] = '^dev\\.www\\.typo3-vorlage\\.at$';
})();
