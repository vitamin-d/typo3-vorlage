<?php

/*
dP                                              dP                             dP
88                                              88                             88
88  .dP  .d8888b. .d8888b. 88d888b.    .d8888b. 88 .d8888b. .d8888b. 88d888b.  88
88888"   88ooood8 88ooood8 88'  `88    88'  `"" 88 88ooood8 88'  `88 88'  `88  dP
88  `8b. 88.  ... 88.  ... 88.  .88    88.  ... 88 88.  ... 88.  .88 88    88
dP   `YP `88888P' `88888P' 88Y888P'    `88888P' dP `88888P' `88888P8 dP    dP  oo
                           88
                           dP

 Notes:
   - only settings common to all instances belong here
   - instance-specific settings go into the instance's AdditionalConfiguration
   - clean up thoroughly after using Install Tool or Extension Manager
 */

return [
    'BE' => [
        'explicitADmode' => 'explicitAllow',
        'loginSecurityLevel' => 'normal',
    ],
    'EXT' => [
        'extConf' => [
            'assets' => 'a:0:{}',
            'backend' => 'a:0:{}',
            'beuser' => 'a:0:{}',
            'columns' => 'a:0:{}',
            'documentation' => 'a:0:{}',
            'extensionmanager' => 'a:2:{s:21:"automaticInstallation";s:1:"1";s:11:"offlineMode";s:1:"0";}',
            'filemetadata' => 'a:0:{}',
            'fix_imageviewhelper' => 'a:0:{}',
            'fluid_styled_content' => 'a:0:{}',
            'info' => 'a:0:{}',
            'info_pagetsconfig' => 'a:0:{}',
            'l10n' => 'a:0:{}',
            'linkvalidator' => 'a:0:{}',
            'lowlevel' => 'a:0:{}',
            'opendocs' => 'a:0:{}',
            'realurl' => 'a:6:{s:10:"configFile";s:34:"typo3conf/RealurlConfiguration.php";s:14:"enableAutoConf";s:1:"0";s:14:"autoConfFormat";s:1:"0";s:17:"segTitleFieldList";s:0:"";s:12:"enableDevLog";s:1:"0";s:10:"moduleIcon";s:1:"1";}',
            'recycler' => 'a:0:{}',
            'reports' => 'a:0:{}',
            'rte_ckeditor' => 'a:0:{}',
            'scheduler' => 'a:4:{s:11:"maxLifetime";s:4:"1440";s:11:"enableBELog";s:1:"0";s:15:"showSampleTasks";s:1:"1";s:11:"useAtdaemon";s:1:"0";}',
            'setup' => 'a:0:{}',
            'site_package' => 'a:0:{}',
            'sys_action' => 'a:0:{}',
            'sys_note' => 'a:0:{}',
            'taskcenter' => 'a:0:{}',
            'tstemplate' => 'a:0:{}',
            'typo3_console' => 'a:0:{}',
            'unroll' => 'a:7:{s:7:"buttons";s:50:"_savedok,_savedokview,_savedoknew,_saveandclosedok";s:6:"unroll";s:1:"1";s:13:"showLabelSave";s:1:"1";s:20:"showLabelSaveDokView";s:1:"1";s:19:"showLabelSaveDokNew";s:1:"1";s:21:"showLabelSaveAndClose";s:1:"1";s:17:"allowUserSettings";s:1:"1";}',
            'vhs' => 'a:1:{s:20:"disableAssetHandling";s:1:"0";}',
            'viewpage' => 'a:0:{}',
            'wizard_crpages' => 'a:0:{}',
            'workspaces' => 'a:0:{}',
        ],
    ],
    'EXTCONF' => [
        'lang' => [
            'availableLanguages' => [
                'de',
            ],
        ],
    ],
    'FE' => [
        'hidePagesIfNotTranslatedByDefault' => true,
        'loginSecurityLevel' => 'normal',
        'pageNotFound_handling' => '/404/',
    ],
    'SYS' => [
        'ddmmyy' => 'Y-m-d',
        'doNotCheckReferer' => true,
        'exceptionalErrors' => 20480,
        'isInitialDatabaseImportDone' => true,
        'isInitialInstallationInProgress' => false,
        'loginCopyrightWarrantyProvider' => 'VITAMIN D GmbH',
        'loginCopyrightWarrantyURL' => 'http://www.vitd.at/',
        'phpTimeZone' => 'Europe/Vienna',
        'sitename' => 'TYPO3 Vorlage',
        'textfile_ext' => 'txt,html,htm,css,tmpl,js,sql,xml,csv,xlf,yaml,yml,t3s,ts,tsc,typoscript',
    ],
];
