#!/usr/bin/env bash
#
# Provision testing environment: Set up vhosts for apache
#
# Ludwig Rafelsberger <lr@vitd.at>, VITAMIN D GmbH
#
# See:
# https://wiki.archlinux.org/index.php/Apache_HTTP_Server


#
# Set up vhosts for apache
#
main() {

	cat > /etc/httpd/conf/extra/httpd-vhosts.conf <<EOF
<VirtualHost *:80>
	ServerAdmin lr@vitd.at
	DocumentRoot "/srv/http/www/web"
	ServerName dev.www.typo3-vorlage.at
	ErrorLog "/var/log/httpd/error_log"
	CustomLog "/var/log/httpd/access_log" common

	<Directory /srv/http/www/web>
		AllowOverride All
	</Directory>
</VirtualHost>
EOF


	if grep --extended-regex --quiet '^#?Include conf/extra/httpd-vhosts.conf$' /etc/httpd/conf/httpd.conf ; then
		sed --regexp-extended --in-place 's@#?Include conf/extra/httpd-vhosts.conf@Include conf/extra/httpd-vhosts.conf@' /etc/httpd/conf/httpd.conf
	else
		echo 'Include conf/extra/httpd-vhosts.conf' >> /etc/httpd/conf/httpd.conf
	fi


	if grep --extended-regex --quiet '^#?LoadModule proxy_module modules/mod_proxy.so$' /etc/httpd/conf/httpd.conf ; then
		sed --regexp-extended --in-place 's@#?LoadModule proxy_module modules/mod_proxy.so@LoadModule proxy_module modules/mod_proxy.so@' /etc/httpd/conf/httpd.conf
	else
		echo 'LoadModule proxy_module modules/mod_proxy.so' >> /etc/httpd/conf/httpd.conf
	fi

	if grep --extended-regex --quiet '^#?LoadModule proxy_fcgi_module modules/mod_proxy_fcgi.so$' /etc/httpd/conf/httpd.conf ; then
		sed --regexp-extended --in-place 's@#?LoadModule proxy_fcgi_module modules/mod_proxy_fcgi.so@LoadModule proxy_fcgi_module modules/mod_proxy_fcgi.so@' /etc/httpd/conf/httpd.conf
	else
		echo 'LoadModule proxy_fcgi_module modules/mod_proxy_fcgi.so' >> /etc/httpd/conf/httpd.conf
	fi

	if grep --extended-regex --quiet '^#?Include conf/extra/php-fpm.conf$' /etc/httpd/conf/httpd.conf ; then
		sed --regexp-extended --in-place 's@#?Include conf/extra/php-fpm.conf@Include conf/extra/php-fpm.conf@' /etc/httpd/conf/httpd.conf
	else
		echo 'Include conf/extra/php-fpm.conf' >> /etc/httpd/conf/httpd.conf
	fi

	cat > /etc/httpd/conf/extra/php-fpm.conf <<EOF
<FilesMatch \.php$>
	SetHandler "proxy:unix:/run/php-fpm/php-fpm.sock|fcgi://localhost/"
</FilesMatch>
<IfModule dir_module>
    DirectoryIndex index.html
    DirectoryIndex index.php
</IfModule>
EOF

	if grep --extended-regex --quiet '^#?Include conf/extra/php-fpm.conf$' /etc/httpd/conf/httpd.conf ; then
		sed --regexp-extended --in-place 's@#?Include conf/extra/php-fpm.conf@Include conf/extra/php-fpm.conf@' /etc/httpd/conf/httpd.conf
	else
		echo 'Include conf/extra/php-fpm.conf' >> /etc/httpd/conf/httpd.conf
	fi


	systemctl restart httpd
}





main "$@"
# vim: ts=4
