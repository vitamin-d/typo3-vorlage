#!/usr/bin/env bash
#
# Provision testing environment: Set up cron jobs
#
# Ludwig Rafelsberger <lr@vitd.at>, VITAMIN D GmbH
#
# See:
# https://wiki.archlinux.org/index.php/Systemd/Timers
# https://wiki.archlinux.org/index.php/Systemd#Writing_unit_files


#
# Set up cron jobs
#
main() {

	cat > /etc/systemd/system/typo3-scheduler.service <<EOF
[Unit]
Description=Run TYPO3 scheduler

[Service]
Type=oneshot
ExecStart=/srv/http/www/vendor/bin/typo3cms scheduler:run
User=http

[Install]
WantedBy=multi-user.target
EOF

	cat > /etc/systemd/system/typo3-scheduler.timer <<EOF
[Unit]
Description=Run TYPO3 scheduler regularly and on boot

[Timer]
OnBootSec=15s
OnUnitActiveSec=15min

[Install]
WantedBy=timers.target
EOF
	systemctl enable typo3-scheduler.timer
	systemctl start typo3-scheduler.timer
}





main "$@"
# vim: ts=4
