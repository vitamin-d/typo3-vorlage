#!/usr/bin/env bash
#
# Provision testing environment: Install vendor files (in RAM for performance)
#
# Ludwig Rafelsberger <lr@vitd.at>, VITAMIN D GmbH



#
# Ensure existent, up-to-date composer
#
main() {
	# we have git repositories as source for at least one composer package: thus we need git
	if ! pacman --query --quiet --search '^git$' >/dev/null ; then
		sudo pacman --sync --noconfirm git
	fi

	export COMPOSER_DISABLE_XDEBUG_WARN=1
	# keep composer up to date
	sudo pacman --sync --noconfirm --needed composer

	sudo mkdir -p /srv/http/.composer
	sudo chown http:http /srv/http/.composer

	# mount vendor directory in ram
	sudo mkdir -p /srv/http/www/vendor
	if ! mountpoint --quiet /srv/http/www/vendor ; then
		sudo mount -t tmpfs -o size=128M /dev/ram /srv/http/www/vendor
	fi
	sudo chown -R http:http /srv/http/www/vendor

	# install vendor files
	composer --working-dir=/srv/http/www --ignore-platform-reqs --no-progress --no-suggest --optimize-autoloader install
}


main "$@"
# vim: ts=4
