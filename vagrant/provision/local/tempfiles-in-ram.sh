#!/usr/bin/env bash
#
# Provision testing environment: Hold temporary files in memory and create required directory structure in typo3temp
#
# Ludwig Rafelsberger <lr@vitd.at>, VITAMIN D GmbH



#
# Set up this virtual machine
#
main() {
	mkdir -p /srv/http/www/web/typo3temp
	if ! mountpoint --quiet /srv/http/www/web/typo3temp ; then
		mount -t tmpfs -o size=128M /dev/ram /srv/http/www/web/typo3temp
	fi
	chown -R http:http /srv/http/www/web/typo3temp

	sudo --user=http mkdir -p /srv/http/www/web/typo3temp/{cs,GB,llxml,locks,pics,temp,_processed_,assets/{compressed,css,js,images,_processed_},var/{charset,Cache,locks}}
	sudo --user=http touch /srv/http/www/web/typo3temp/index.html
	if [ ! -r /srv/http/www/web/typo3temp/var/.htaccess ] ; then
		sudo --user=http tee /srv/http/www/web/typo3temp/var/.htaccess >/dev/null <<-"EOF"
			# This file restricts access to the typo3temp/var/ directory. It is
			# meant to protect temporary files which could contain sensible
			# information. Please do not touch.

			# Apache < 2.3
			<IfModule !mod_authz_core.c>
			    Order allow,deny
			    Deny from all
			    Satisfy All
			</IfModule>

			# Apache ≥ 2.3
			<IfModule mod_authz_core.c>
			    Require all denied
			</IfModule>
		EOF
	fi
}



main "$@"
# vim: ts=4
