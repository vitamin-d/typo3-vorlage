#!/usr/bin/env bash
#
# Provision testing environment: Configure ssh access to live host
#
# Ludwig Rafelsberger <lr@vitd.at>, VITAMIN D GmbH



#
# Configure ssh access to live host
#
main() {
	add_ssh_hostkey www.typo3-vorlage.at 'dsa,ecdsa,ed25519,rsa'

	cat > /home/vagrant/.ssh/config <<EOF
Host www.typo3-vorlage.at
	User typo3-vorlage_ssh_user
EOF
	chown vagrant:vagrant /home/vagrant/.ssh/config
	chmod 0600 /home/vagrant/.ssh/config

	echo "Please make sure you can connect as typo3-vorlage_ssh_user@www.typo3-vorlage.at from your host(!!)"
	echo 'and have an SSH agent running, so that the guest machine can use your SSH credentials'
	echo 'to connect to the live webserver.'
}


#
# Retrieve ssh host keys and add them to the vagrants' known_hosts
#
# $1 - List of hostnames to have their keys added (Space separated)
# $2 - SSH key types to fetch from each host (Comma separated)
add_ssh_hostkey() {

	local hosts="$1"; shift
	local algos="$1"; shift

	local known_hosts_file="/home/vagrant/.ssh/known_hosts"

	if [ ! -e ${known_hosts_file} ] ; then
		touch ${known_hosts_file}
		chown vagrant:vagrant ${known_hosts_file}
		chmod 600 ${known_hosts_file}
	fi

	(cat ${known_hosts_file} ; ssh-keyscan -t ${algos} ${hosts}) \
		| grep -v -E '(^#|^no hostkey alg$)' \
		| sort -u -o ${known_hosts_file}
}

main "$@"
# vim: ts=4
