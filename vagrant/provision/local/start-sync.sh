#!/usr/bin/env bash
#
# Provision testing environment: Start the (external) data sync script
#
# Ludwig Rafelsberger <lr@vitd.at>, VITAMIN D GmbH



#
# Sync data from live webhost via external script
#
# Must be started as vagrant, not root!
#
main() {
	/bin/bash /vagrant/sync/down.sh
}





main "$@"
# vim: ts=4
