#!/usr/bin/env bash
#
# Provision testing environment: Set the TYPO3 Context to "Development"
#
# Ludwig Rafelsberger <lr@vitd.at>, VITAMIN D GmbH



#
# Set the TYPO3 Context to "Development"
#
main() {
	# https://wiki.archlinux.org/index.php/Environment_variables
	touch /etc/environment
	if ! grep -q 'TYPO3_CONTEXT="Development"' /etc/environment ; then
		echo 'TYPO3_CONTEXT="Development"' >> /etc/environment
		source /etc/environment
	fi

	touch /etc/profile.d/environment.sh
	if ! grep -q 'TYPO3_CONTEXT="Development"' /etc/profile.d/environment.sh ; then
		echo 'TYPO3_CONTEXT="Development"' >> /etc/profile.d/environment.sh
	fi
	chmod +x /etc/profile.d/environment.sh
}


main "$@"
# vim: ts=4
