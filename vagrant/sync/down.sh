#!/usr/bin/env bash

main() {
    DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

    echo 'Syncing data from live webhost:'

    echo -e "\t- analyzing database tables"
	local fulltables=$(\
	    ssh www.typo3-vorlage.at "echo \"show tables;\" | mysql --defaults-extra-file=~/.my.cnf --host=localhost \"typo3-vorlage_website_live_database_name\"" \
	    | grep \
	        --extended-regexp '^cf_|^cache_|sys_(history|log)|tx_extensionmanager_domain_model_extension|tx_realurl_(pathcache|uniqalias_cache_map|urlcache)' \
	        --invert-match \
	    | grep \
	        --invert-match \
	        --extended-regexp '^Tables_in_' \
	    | tr '\n' ' ' \
	)

	local emptytables=$(\
	    ssh www.typo3-vorlage.at "echo \"show tables;\" | mysql --defaults-extra-file=~/.my.cnf --host=localhost \"typo3-vorlage_website_live_database_name\"" \
	    | grep \
	        --extended-regexp '^cf_|^cache_|sys_(history|log)|tx_extensionmanager_domain_model_extension|tx_realurl_(pathcache|uniqalias_cache_map|urlcache)'\
	    | tr '\n' ' ' \
	)

    echo -e "\t- syncing normal database tables"
	ssh www.typo3-vorlage.at "mysqldump --defaults-extra-file=~/.my.cnf --host=localhost \"typo3-vorlage_website_live_database_name\" ${fulltables} | bzip2 --compress" \
	    | bzip2 --decompress \
	    | mysql --defaults-extra-file=/home/vagrant/.my.cnf "dev"
    echo -e "\t- syncing structure of temporary database tables"
	ssh www.typo3-vorlage.at "mysqldump --defaults-extra-file=~/.my.cnf --host=localhost --no-data \"typo3-vorlage_website_live_database_name\" ${emptytables}" \
	    | mysql --defaults-extra-file=/home/vagrant/.my.cnf "dev"


	echo -e "\t- rewriting domain entries"
	mysql --defaults-extra-file=/home/vagrant/.my.cnf "dev" <<-'EOF'
		update sys_domain set domainName='dev.www.typo3-vorlage.at' where domainName='www.typo3-vorlage.at';
		EOF


    echo -e "\t- syncing user data"
    rsync \
		--recursive \
		--links \
		--delete \
		--temp-dir=/tmp \
		--exclude-from $(readlink -m ${DIR}/down.rsync-exclude) \
		www.typo3-vorlage.at:/path/to/typo3-vorlage_web-dir/ \
		/srv/http/www/


	echo -e "\t- flushing local caches"
	sudo -u http TYPO3_CONTEXT=Development /usr/bin/php /srv/http/www/vendor/bin/typo3cms cache:flush


	echo -e "\t- ensuring proper folder structure"
	sudo -u http TYPO3_CONTEXT=Development /usr/bin/php /srv/http/www/vendor/bin/typo3cms install:fixfolderstructure

    echo "Done syncing data from live webhost"
}




main "$@"
# vim: ts=4
