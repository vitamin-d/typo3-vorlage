#!/usr/bin/env bash
#
# Publish the current project content to live webserver
#

#
# Publish the project
#
# $1 - target host (Note: ssh settings (user, port, key, …) belong into your ssh config.)
# $2 - hosting Base directory (vhost root) on target host. Avoid trailing slash! - you might destroy the target system
# $3 - hosting PHP path and binary. Must match the PHP version used by the webserver
# $4 - Group both the web server and the ssh user are part of, so that cross-writability can be achieved. If the web
#      server runs as the ssh user then provide the main group of that user.
#
main() {

	local hosting_host="$1"; shift
	local hosting_basedir="$1"; shift
	local hosting_php="$1"; shift
	local hosting_shared_group="$1"; shift
	DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

	echo 'Publishing project:'

	echo -e '\tLocking backend and flushing caches'
	ssh "${hosting_host}" "(cd "\""${hosting_basedir}"\"" ; ${hosting_php} vendor/bin/typo3cms backend:lock ; ${hosting_php} vendor/bin/typo3cms cache:flush)"

	echo -e '\tUploading source'
	rsync \
		--recursive \
		--links \
		--delete \
		--temp-dir=/tmp \
		--exclude-from "$(readlink -m "${DIR}/config/publish.rsync-exclude")" \
		"$(readlink -m "${DIR}/../httpdocs")/" \
		"${hosting_host}:${hosting_basedir}/"

	echo -e '\tFixing access rights'
	ssh "${hosting_host}" "(cd "\""${hosting_basedir}"\"" ; chgrp -fR ${hosting_shared_group} web)"
	ssh "${hosting_host}" "(cd "\""${hosting_basedir}"\"" ; ls | grep -v web | xargs chmod -R g+rw,o+r)"
	ssh "${hosting_host}" "(cd "\""${hosting_basedir}/web"\"" ; ls | grep -v -E '(typo3temp|uploads|fileadmin)' | xargs chmod -R g+rw,o+r)"
	ssh "${hosting_host}" "(cd "\""${hosting_basedir}"\"" ; find \$(ls | grep -v web) -type d -exec chmod 0775 \{\} \;)"
	ssh "${hosting_host}" "(cd "\""${hosting_basedir}/web"\"" ; find \$(ls | grep -v -E '(typo3temp|uploads|fileadmin)') -type d -exec chmod 0775 \{\} \;)"
	ssh "${hosting_host}" "(cd "\""${hosting_basedir}"\"" ; chmod u+x,g+x vendor/bin/typo3cms web/typo3/cli_dispatch.phpsh web/typo3/sysext/core/bin/typo3)"

	echo -e '\tUpdating database schema'
	ssh "${hosting_host}" "(cd "\""${hosting_basedir}"\"" ; ${hosting_php} vendor/bin/typo3cms database:updateschema '*.add,*.change')"

	echo -e '\tFlushing caches (again) and unlocking backend'
	ssh "${hosting_host}" "(cd "\""${hosting_basedir}"\"" ; ${hosting_php} vendor/bin/typo3cms cache:flush ; ${hosting_php} vendor/bin/typo3cms backend:unlock)"

	echo 'Done publishing project'
}



main "$@"
# vim: ts=4
