#!/usr/bin/env bash
#
# Finish integration after the publishing
#

#
# Do some publish afterwork
#
# $1 - target host (Note: ssh settings (user, port, key, …) belong into your ssh config.)
# $2 - hosting Base directory (vhost root) on target host. Avoid trailing slash! - you might destroy the target system
# $3 - hosting PHP path and binary. Must match the PHP version used by the webserver
#
main() {
	local hosting_host="$1"; shift
	local hosting_basedir="$1"; shift
	local hosting_php="$1"; shift
	DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

	echo 'Finishing integration'

	echo -e '\tUpdating languages and flushing caches'
	ssh "${hosting_host}" "(cd "\""${hosting_basedir}"\"" ; ${hosting_php} vendor/bin/typo3cms language:update ; ${hosting_php} vendor/bin/typo3cms cache:flush)"

	echo 'Done finishing integration'
}



main "$@"
# vim: ts=4
