# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'fileutils'
require 'yaml'



VAGRANT_DOTFILE_PATH = 'vagrant/.vagrant'
if(ENV['VAGRANT_DOTFILE_PATH'].nil? && '.vagrant' != VAGRANT_DOTFILE_PATH)
	ENV['VAGRANT_DOTFILE_PATH'] = VAGRANT_DOTFILE_PATH
	system('vagrant ' + ARGV.join(' '))
	status = $?.exitstatus
	ENV['VAGRANT_DOTFILE_PATH'] = nil
	exit $?.exitstatus
end



$settings = YAML.load_file('vagrant/dev.yml')['settings']
if (File.file?('vagrant/dev-local.yml'))
	$settings = $settings.merge(Hash(YAML.load_file('vagrant/dev-local.yml')['settings']))
end


Vagrant.configure('2') do |config|
	config.vm.box = $settings['box']


	# use ssh keys from host machine to access live server
	config.ssh.forward_agent = true


	# VirtualBox setting
	config.vm.provider 'virtualbox' do |v|
		v.name = $settings['hostnames'].split(' ')[0]
		v.customize ['modifyvm', :id, '--cpuexecutioncap', $settings['cpuexecutioncap']]
		v.customize ['modifyvm', :id, '--vram', 16]
		v.customize ['modifyvm', :id, '--memory', $settings['memory']]
		v.customize ['modifyvm', :id, '--cpus', $settings['cpus']]
	end


	# Network settings
	config.vm.define $settings['hostnames'].split(' ')[0] do |node|

		node.vm.hostname = $settings['hostnames'].split(' ')[0]

		if $settings['privateip'].length > 0
			node.vm.network :private_network, ip: $settings['privateip'], nic_type: 'virtio'
		else
			node.vm.network :private_network, type: 'dhcp', nic_type: 'virtio'
		end

		if $settings['publicip'].length > 0
			if $settings['publicip'] == 'dhcp'
				node.vm.network 'public_network'
			else
				node.vm.network 'public_network', ip: $settings['publicip']
			end
		end
	end


	# Synced folders
	config.vm.synced_folder '/', '/vagrant', disabled: true
	config.vm.synced_folder 'vagrant', '/vagrant'
	config.vm.synced_folder 'httpdocs', '/srv/http/www'


	# Provision
	config.vm.provision 'shell' do |s|
		s.path = 'https://raw.githubusercontent.com/tantegerda1/provision/master/archlinux/os.sh'
		s.args = [
			$settings['hostnames'].split(' ')[0],
			$settings['hostnames'],
		]
		s.keep_color = true
		s.name = 'os'
	end
	config.vm.provision :shell, :keep_color => true, :path => 'https://raw.githubusercontent.com/tantegerda1/provision/master/archlinux/mariadb.sh', :name => 'mariadb'
	config.vm.provision :shell, :keep_color => true, :path => 'https://raw.githubusercontent.com/tantegerda1/provision/master/archlinux/database.sh', :name => 'database'
	config.vm.provision :shell, :keep_color => true, :path => 'https://raw.githubusercontent.com/tantegerda1/provision/master/archlinux/apache.sh', :name => 'apache'
	config.vm.provision :shell, :keep_color => true, :path => 'https://raw.githubusercontent.com/tantegerda1/provision/master/archlinux/php.sh', :name => 'php'
	config.vm.provision :shell, :keep_color => true, :path => 'https://raw.githubusercontent.com/tantegerda1/provision/master/archlinux/graphicsmagick.sh', :name => 'graphicsmagick'
	config.vm.provision :shell, :keep_color => true, :path => 'https://raw.githubusercontent.com/tantegerda1/provision/master/archlinux/xdebug.sh', :name => 'xdebug'
	config.vm.provision :shell, :keep_color => true, :path => 'vagrant/provision/local/ssh.sh', :name => 'ssh'
	config.vm.provision :shell, :keep_color => true, :path => 'vagrant/provision/local/typo3-context-development.sh', :name => 'typo3-context-development'
	config.vm.provision :shell, :keep_color => true, :path => 'vagrant/provision/local/vhosts.sh', :name => 'vhosts'
	config.vm.provision :shell, :keep_color => true, :path => 'vagrant/provision/local/setup-cronjobs.sh', :name => 'setup-cronjobs'
	config.vm.provision :shell, :keep_color => true, :run => 'always', :path => 'vagrant/provision/local/vendorfiles-in-ram.sh', :name => 'vendorfiles-in-ram', privileged: false
	config.vm.provision :shell, :keep_color => true, :run => 'always', :path => 'vagrant/provision/local/tempfiles-in-ram.sh', :name => 'tempfiles-in-ram'

	config.vm.provision :shell, :keep_color => true, :path => 'vagrant/provision/local/start-sync.sh', :name => 'start-sync', privileged: false
end



if (File.file?('Vagrantfile.local'))
	load('Vagrantfile.local')
end



FileUtils.remove_dir('.vagrant')
